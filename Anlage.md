# Anlage
### Überblick
1. [Designalternativen](#designalternativen)
2. [Usertest](#usertest)
3. [Literaturrecherche zur Navigation](#literaturrecherche-zur-navigation)
4. [Quellenverzeichnis](#quellenverzeichnis)
<br>

### Designalternativen
***

<img width="507" alt="Bildschirmfoto 2022-04-29 um 10 45 24" src="https://user-images.githubusercontent.com/101112426/165912602-7a96fb7b-b069-4645-a22e-eb2026cc0607.png">


**Design 1**                                          
 Bei diesem Designvorschlag (s. linke Möglichkeit im vorherigen Bild) erfolgt die Navigation innerhalb der e-Portfolios über einen "Zurück-/Weiter-Button". Hierbei wird der eingegebene Suchbegriff in chronologischer Reihenfolge von oben nach unten aufgelistet. 
 
<br>

**Design 2**   
 Bei diesem Designvorschlag (s. rechte Möglichkeit im vorherigen Bild) erfolgt die Navigation innerhalb der e-Portfolios über eine Auflistung aller enthaltenen Begriffe des eingegeben Suchbegriffs.  
  



### Usertest
***
**Allgemeines:**
- Unseren Usability-Test haben wir mit insgesamt 4 unabhängigen Testpersonen durchgeführt.
- Bei der Auswahl unserer Testpersonen haben wir darauf geachtet, dass diese bis zu diesem Zeitpunkt die verwendete Word- und PDF-Datei noch nicht kennen. 
- Die ersten fünf Aufgaben, welche wir den Testpersonen gestellt haben, haben wir anhand der möglichen Navigationen (Auflistung und Weiter-Button) der e-Portfolios abgeleitet.
- Die unterschiedlichen Navigationstypen sehen wie folgt aus:
	- Die Word-Datei hat eine Auflistung:   
	<img width="300" alt="Bildschirmfoto 2022 12 25 um 10 45 24" src="https://user-images.githubusercontent.com/98404883/206475333-ffc18612-23aa-450a-9ad3-e3e1f1877fdb.png"> <br>
  	- Die PDF-Datei hat einen Weiter- und Zurück-Button mit der Anzahl an Suchergebnissen: <br> <img width="300" alt="Screenshot 2022 12 24 um 10 45 24" src="https://user-images.githubusercontent.com/98404883/206475922-2b17e4b1-a662-4f26-a105-e8f1878d81e0.png">
- Die sechste Aufgabe untersuchte unsere Farbkombination des Highlightens und Hervorhebens für unsere Webseite. Die erstellte Farbpalette ergab sich vor allem aus der Methode des Vergleichs mit anderen Webseiten. Darin haben wir verschiedene Farbkonstellationen gemischt und uns schließlich auf eine bestimmte Farbpalette geeinigt. Ebenso haben wir Tools ausprobiert, in denen die Farbpaletten anhand eines komplementären Farbschemas ausgesucht werden.
- Die Tests wurden mithilfe eines Laptops oder Computers von den Testpersonen durchgeführt. Hierfür wurden lediglich Androidgeräte verwendet, da die Navigationsmöglichkeit "Auflistung" in Word-Dateien bei MacBooks nicht exisitiert. 
- Während der Durchführung der Tests wurden sowohl das laute Denken, die Klickzahlen als auch die Zeit der Testpersonen dokumentiert und gestoppt. 

<br>

**Aufgaben an die Testpersonen:**   
 
 - Hinweis, welchen die Testpersonen vor Durchführung des Usertests von uns erhielten:   
 "Für die Vorschau der beigefügten PDF-Datei bitte lediglich das  einheitliche Vorschauprogramm Adobe Acrobat Reader DC für die PDF-Datei verwenden."

- Die Aufgaben 1 bis 5 wurden jeweils mit einer PDF- und mit einer Word-Datei durchgeführt, d.h sie wurden zweimal bearbeitet. Hierbei handelte es sich um die e-Portfolio Vorlage für unsere Webseite (s. https://serdareviceportfolioit3.wordpress.com/). 

- Die 6. Aufgabe wurde mit den folgenden Webseiten bearbeitet: 
<br>
Farbkombination 1: https://clinquant-boba-a8848f.netlify.app/
Farbkombination 2: https://exquisite-lebkuchen-465c63.netlify.app/


**Aufgaben:** 
Die folgenden Aufgaben gelten sowohl für die PDF- als auch für die Word-Datei.
<br>

*Aufgabe 1*
 - Suche in dem dir vorliegenden Dokument nach dem Wort Funktion
  → Wie viele Ergebnisse findest du? 
<br>

*Aufgabe 2*
 - Suche in dem dir vorliegenden Dokument nach dem 6. Wort Struktur
  → Wähle das 6. Struktur aus     
<br>

*Aufgabe 3*
- Springe vom dritten Wort Übung zum 15. Wort Übung
<br>

*Aufgabe 4* 
- Suche das Wort Code im Kontext der Schleifen
<br>

*Aufgabe 5*
- Was gefällt dir gut/schlecht bei der jeweiligen Ergebnisanzeige? 
- Welche Ergebnisanzeige findest du besser/schlechter und weshalb?
<br>

*Aufgabe 6*
- Öffne die beiden Links. 
<br>
Farbkombination 1: https://clinquant-boba-a8848f.netlify.app/
Farbkombination 2: https://exquisite-lebkuchen-465c63.netlify.app/

- Bei welcher Farbkombination erkennst du besser den Zusammenhang der gefärbten Schlagwörter und dem zugehörigen Inhalt?
Bitte ankreuzen:  Farbkombination 1 () oder Farbkombination 2 ( )

- Welche Farben harmonieren für dich persönlich besser?

Bitte ankreuzen: Farbkombination 1 ( ) oder Farbkombination 2 ( )
<br>
<br>

**Ergebnisse des Usertests:**
 
 Übersicht der Ergebnisse:
 <br>
 
<!DOCTYPE html>

<html>
	
</head>

<body>
<table cellspacing="0" border="0">
	<colgroup width="128"></colgroup>
	<colgroup width="244"></colgroup>
	<colgroup width="365"></colgroup>
	<colgroup width="455"></colgroup>
	<colgroup width="393"></colgroup>
	<tr>
		<td height="26" align="center"><b><font face="Liberation Serif">Aufgaben</font></b></td>
		<td align="center"><b><font face="Liberation Serif">Teilnehmer 1</font></b></td>
		<td align="center"><b><font face="Liberation Serif">Teilnehmer 2</font></b></td>
		<td align="center"><b><font face="Liberation Serif">Teilnehmer 3</font></b></td>
		<td align="center"><b><font face="Liberation Serif">Teilnehmer 4</font></b></td>
	</tr>
	<tr>
		<td height="180" align="left" valign=top><font face="Liberation Serif">Aufgabe 1<br><br>
			PDF
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			Word</font></td>
		<td align="left" valign=top><font face="Liberation Serif">Korrekte Ergebnisanzahl gefunden:<br>28 nur für das Wort Funktion<br><br><br><br><br>Korrekte Ergebnisanzahl gefunden:<br>46 Varianten des Wortes Funktion (Wortstamm)</font></td>
		<td align="left" valign=top><font face="Liberation Serif">Korrekte Ergebnisanzahl gefunden:<br>28 (Wort „Funktion“; nicht „Funktionen“ etc.)<br><br>Korrekte Ergebnisanzahl gefunden:<br>46 (auch Wörter wie „Funktionen“ beinhaltet)</font></td>
		<td align="left" valign=top><font face="Liberation Serif">Korrekte Ergebnisanzahl gefunden:<br>45<br><br><br><br><br><br><br><br>Korrekte Ergebnisanzahl gefunden:<br>46 (“Hä wie können es zwei verschiedene Anzahlen sein?”)</font></td>
		<td align="left" valign=top><font face="Liberation Serif">Korrekte Ergebnisanzahl gefunden:<br>45<br><br><br><br><br><br><br>Korrekte Ergebnisanzahl gefunden:<br>46 <br>(nicht klar warum es eins mehr gibt) </font></td>
	</tr>
	<tr>
		<td height="180" align="left" valign=top><font face="Liberation Serif">Aufgabe 2<br><br>PDF<br><br><br>                                                                  Word</font></td>
		<td align="left" valign=top><font face="Liberation Serif">6. Wort wurde gefunden und ausgewählt: <br>Ja<br><br>6. Wort wurde gefunden und ausgewählt: <br>Ja</font></td>
		<td align="left" valign=top><font face="Liberation Serif">6. Wort wurde gefunden und ausgewählt: <br>Ja<br><br>6. Wort wurde gefunden und ausgewählt: <br>Ja</font></td>
		<td align="left" valign=top><font face="Liberation Serif">6. Wort wurde gefunden und ausgewählt: <br>Finde ich nicht – nur drei (häh warum, wo soll es sein?)<br><br><br>6. Wort wurde gefunden und ausgewählt: <br>Finde ich nicht – nur vier (auch hier eher frustriert)</font></td>
		<td align="left" valign=top><font face="Liberation Serif">6. Wort wurde gefunden und ausgewählt: <br>wurden nur 3 Treffer gefunden<br><br><br>6. Wort wurde gefunden und ausgewählt: <br>auch hier wurden nur 4 Treffer gefunden</font></td>
	</tr>
	<tr>
		<td height="247" align="left" valign=top><font face="Liberation Serif">Aufgabe 3<br><br>PDF<br><br><br><br>                                                                                                                                                                        Word</font></td>
		<td align="left" valign=top><font face="Liberation Serif">Sprung vom dritten zum 15. Wort hat funktioniert:<br>Ja<br><br><br>Sprung vom dritten zum 15. Wort hat funktioniert:<br>Ja</font></td>
		<td align="left" valign=top><font face="Liberation Serif">Sprung vom dritten zum 15. Wort hat funktioniert:<br>Ja<br><br><br>Sprung vom dritten zum 15. Wort hat funktioniert:<br>Ja</font></td>
		<td align="left" valign=top><font face="Liberation Serif">Sprung vom dritten zum 15. Wort hat funktioniert:<br>Ich brauchte 16 Klicks, weil ich von den 20 Möglichkeiten rückwärts zum 15. Wort Übung gesprungen bin – wollte es mir leichter machen..<br><br>Sprung vom dritten zum 15. Wort hat funktioniert:<br>Gefunden – brauchte 18 Klicks  </font></td>
		<td align="left" valign=top><font face="Liberation Serif">Sprung vom dritten zum 15. Wort hat funktioniert:<br>hat geklappt, allerdings ist es kein springen sondern durchklicken <br><br><br>Sprung vom dritten zum 15. Wort hat funktioniert:<br>gefunden - allerdings wurde durchklicken als leichter empfunden, da sonst sowieso gezählt werden muss </font></td>
	</tr>
	<tr>
		<td height="247" align="left" valign=top><font face="Liberation Serif">Aufgabe 4<br><br>PDF<br><br><br><br><br>                                                                                                 Word</font></td>
		<td align="left" valign=top><font face="Liberation Serif">Wort wurde erfolgreich im Kontext gefunden:<br>Ja<br><br>Wort wurde erfolgreich im Kontext gefunden:<br>Ja</font></td>
		<td align="left" valign=top><font face="Liberation Serif">Wort wurde erfolgreich im Kontext gefunden:<br>Ja<br><br>Wort wurde erfolgreich im Kontext gefunden:<br>Ja</font></td>
		<td align="left" valign=top><font face="Liberation Serif">Wort wurde erfolgreich im Kontext gefunden:<br>Gefunden – ist an 13. Stelle und habe alle durchgeklickt und den Text überflogen → später dann nur die Überschriften<br><br><br>Wort wurde erfolgreich im Kontext gefunden:<br>Viel schneller gefunden – die Texte im Word links überflogen und habe das Wort viel schneller gefunden → wenige Klicks, denn runter gescrollt                           </font></td>
		<td align="left" valign=top><font face="Liberation Serif">Wort wurde erfolgreich im Kontext gefunden:<br>gefunden, durch erweiterte Suche kein Ergebnis, nur gefunden durch durchklicken <br><br>Wort wurde erfolgreich im Kontext gefunden:<br>Gefunden, durch die Liste links einen schnelleren Überblick </font></td>
	</tr>
	<tr>
		<td height="555" align="left" valign=top><b><font face="Liberation Serif">Aufgabe 5</font></b></td>
		<td colspan=4 align="left" valign=top><font face="Liberation Serif">Was gefällt dir gut/schlecht bei der jeweiligen Ergebnisanzeige (Ergebnisanzeige von PDF- und Word-Datei vergleichen)? <br>Beim Adobe Reader wird zusätzlich zu Word angezeigt, ob das Wort in anderen Varianten vorkommt und wie häufig (z.B. Funktionen (6), Funktionsaufruf (1), …). Außerdem kann direkt die Wortvariante ausgewählt werden. *(Teilnehmer 1)* <br><br>Bei Word finde ich es gut, dass man den restlichen Kontext des Wortes in der Ergebnisanzeige sieht und ob es sich bei dem gesuchten Wort z.B. um eine Überschrift handelt. Bei PDF-Datei wird die Anzahl der Suchergebnisse direkt angezeigt, das ist gut. Bei Word erst, wenn man ein Ergebnis anklickt, das ist nicht so gut. Bei Word ist es außerdem schwieriger, die Nummer des Ergebnisses direkt zu finden, wie z.B. bei Aufgabe 3. Bei PDF ist die gesamte Suchfunktion sehr klein gehalten, da gefällt mir Word besser. *(Teilnehmer 2)* <br><br>PDF: gut = man kann leicht zum jeweiligen Wort springen vs. schlecht = das Wort wird nicht im Kontext angezeigt <br>Word: gut = leichte Eingabe und dass das Wort links im Kontext sowie im Satz in der Datei präsentiert wird vs. schlecht = manchmal dann doch mehr klicken, außer man scrollt *(Teilnehmer 3)* <br><br>PDF: sehr übersichtlich, einfach gehalten, wenig Zusatzinformationen die verwirren, Vorschläge mit Anzahl sehr gut <br>WORD: zu viele Informationen, Programm ist nicht auf die Suche ausgelegt *(Teilnehmer 4)* <br><br><br><br>Welche Ergebnisanzeige findest du besser/schlechter und weshalb?<br>Die Suchfunktion im Adobe Reader ist komfortabler und bietet mehr Möglichkeiten als Word. *(Teilnehmer 1)* <br><br>Beide haben Vor- und Nachteile. Ich würde aufgrund dessen nicht behaupten, dass eine Ergebnisanzeige besser oder schlechter ist, sondern dass man beide optimieren bzw. verbessern kann. *(Teilnehmer 2)* <br><br>Die bei Word: besser → ist flexibler und einfacher zu verstehen bzw. zu bedienen *(Teilnehmer 3)* <br><br>PDF Reader ist besser, weil aufgeräumter (übersichtlicher) *(Teilnehmer 4)* </font></td>
		</tr>
	<tr>
		<td height="357" align="left" valign=top><b><font face="Liberation Serif">Aufgabe 6</font></b></td>
		<td colspan=4 align="left" valign=top><font face="Liberation Serif">Bei welcher Farbkombination erkennst du besser den Zusammenhang der gefärbten Schlagwörter und dem zugehörigen Inhalt?<br>Wenn die rote Umrandung der Felder in der Grafik weg ist, muss der Kopf einen Zusammenhang zwischen rotem Feld und lila Überschrift herstellen. Schwieriger als bei gleicher Farbe (Farbkombi 1) *(Teilnehmer 1)* <br><br>Gleiche Farbe. Zusammenhang einfach erkennbar. Außerdem ist das rot heller und die Schrift in den Feldern besser lesbar (Farbkombi 2) *(Teilnehmer 2)* <br><br>ca. 20 Klicks und zwischen den beiden Seiten 5 mal gewechselt) *(Teilnehmer 3)* <br><br>Anzahl Farbkombination 1: ||<br>Anzahl Farbkombination 2: ||<br><br>Welche Farben harmonieren für dich persönlich besser?<br> Warum lila? Das rot der markierten Felder in der Grafik ist dunkler als bei Farbkombination 2 und daher der schwarze Text schlechter lesbar (Farbkombi 1) *(Teilnehmer 1)* <br><br>Einheitliche Farbe sinnvoll (Farbkombi 2) *(Teilnehmer 2)* <br><br>(nochmal ca. 5 mal geklickt, um es mir besser einzuprägen) *(Teilnehmer 3)* <br><br>Anzahl Farbkombination 1: |<br>Anzahl Farbkombination 2: |||</font></td>
		</tr>
</table>
<!-- ************************************************************************** -->
</body>

<br>

**Zusammenfassung der Ergebnisse:**

- Alle vier Testpersonen haben lediglich bei der ersten Aufgabe über die Word-Datei die korrekte Ergebnisanzahl gefunden.
- Negatives Feedback gab es bei der zweiten Aufgabe, hier wurden die gesuchten Wörter größtenteils nicht gefunden, was eher zu Frustation als zu Erfolgsmomenten führte.
- Positives Feedback gab es hingegen bei der dritten und vierten Aufgabe. Hier hat sowohl bei der PDF- als auch bei der Word-Datei der Sprung bei allen vier Testpersonen funktioniert. Auch die gesuchten Wörter im Kontext wurden bei beiden Dateien und allen vier Usern erfolgreich gefunden.  
- Die fünfte Aufgabe mit den offen gestellten Fragen zeigte vor allem, dass, die PDF-Datei eine übersichtlichere und eindeutigere Ergebnisanzeige (zusätzliche Anzeige der Wortfamilie) hat. Die Word-Datei hingegen zeigt den gesamten Kontext, in welchen das gesuchte Wort eingebettet ist, an. Hier wird jedoch auch mehr Zeit zum Suchen benötigt. 
- Des weiteren stellte sich heraus, dass sowohl bei der ersten als auch zweiten wählbaren Farbkombination der Zusammenhang zwischen dem gefärbtem Inhalt und Schlagwort ersichtlich war. Hier gab es einen Gleichstand bei den Usern. Hingegen wurde die zweite Farbkombination eindeutig als harmonierendere Farbkombination ausgewählt. 

<br>

**Begründung der Entscheidung:** 
 
Unsere Gruppe hat sich aufgrund der Userbefragung, der Literaturrecherche (s. unten) und damit verbundenen Vorteile für die "Zurück/Weiter-Button-Funktion" (s. PDF-Datei) entschieden. Besonders ausschlaggebend hierfür waren die positiveren Antworten der offen gestellten Fragen in Aufgabe fünf. Hier gaben zwei der vier Testpersonen an, dass sie die Ergebnisanzeige der PDF-Datei besser finden. Nur eine Testperson entschied sich für die Ergebnisanzeige der Word-Datei und eine Testperson antwortete neutral. 
Bei der Farbkombination haben wir uns für die zweite Kombination entschieden (Farbkombination 2: https://exquisite-lebkuchen-465c63.netlify.app/), da diese mehrheitlich ausgewählt wurde. 

Außerdem haben wir uns im Laufe unseres Projekts zusätzlich für ein blinkendes rectangle (Rechteck) entschieden. Das rectangle gibt ein optisches Feedback bei nicht vorhandener Verknüpfung. Das Blinken zeigt das Abheben zu vergebenen Konzepten.
Die Wahl der Farbe für das Blinken ist auf die Farbe dunkelgrau gefallen. Die Entscheidung dieser Farbe wurde jedoch lediglich von den Gruppenmitgliedern selbst, uns 4, getroffen.
Diesbezüglich könnte ein weiterer Usertest durchgeführt werden, um das Zusmmenspiel der Farbkombination 2 und der Blinkfarbe zu erheben oder auch die Sinnhhaftigkeit und Ausprägung des Blinkens (Text oder Rechteck blinkt) zu untersuchen. 


### Literaturrecherche zur Navigation
***
**Navigation über Zurück-und Weiter-Button:**

Bei der Platzierung des Weiter-Buttons gibt es Unterschiede. Laut Untersuchungen wird der Weiter-Button mehr von den Usern genutzt, wenn er links angeheftet wird. Hingegen hat die Benutzung von Pfeilen oder Wörtern keinen Unterschied bei der Benutzung der Schaltfläche. Auch ist dert Zurück-Button von großer Bedeutung, wenn dieser nicht umgesetzt wird, sind Abbrüche bei den Usern deutlich höher. Außerdem sind die Abbrüche zusätzlich geringer, wenn der Zurück-Button unter dem Weiter-Button platziert wird, als wenn diese nebeneinander (Zurück-Button links und Weiter-Button rechts) liegen. Es wird deutlich, möchte man weniger Abbrüche bei den Usern erzielen, sollten Zurück- und Weiter-Buttons verwendet werden (Couper, Baker & Mechling, 2011).

<br>

**Navigation über Auflistung der (gesuchten) Begriffe:**
 
Der Suchvorgang bei der Navigationsmöglichkeit über eine Auflistung der (gesuchten) Begriffe erfolgt so, dass der User einen Suchbegriff eingibt und als Ergebnis eine Auflistung der entsprechenden Treffer erscheint, welche mit dem eingegeben Suchbegriff übereinstimmen (Kaul, 1998).




### Quellenverzeichnis
***
1) Gilarski, Katharina; Müller, Verena; Nissen, Martin (2020): Mapping-Techniken zur Unterstützung des wissenschaftlichen Arbeitens. https://archiv.ub.uni-heidelberg.de/volltextserver/28619/1/UBHD_Mapping-Techniken.pdf. [Letzter Zugriff: 31.03.2022].

2) Pädagogische Hochschule Weingarten (2020): Handreichung zur Portfolioprüfung IT-3 V1.2. 
https://www.moopaed.de/moodle/pluginfile.php/579635/mod_resource/content/0/Handreichung%20zur%20Portfolioprüfung%20IT-3-V1.2.pdf. [Letzter Zugriff: 31.03.2022].

3) Couper, M. P., Baker, R., & Mechling, J. (2011). Placement and design of navigation buttons in web surveys. Survey Practice, 4(1), 3054.

4) Kaul, P. (1998): Navigation im W W W. http://peterkaul.de/navigation_www.pdf. [Letzter Zugriff: 29.04.2022].

<br>
<br>
