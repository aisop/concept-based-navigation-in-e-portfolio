$(document).ready(function () {
  $("#previous").hide(); //hide previous btn on startup
  $("#next").hide(); //hide next btn on startup
});

// searching for groups with rect elements
var $groups = $("#cmap g").filter(function () {
  return $(this)[0].innerHTML.includes("<rect"); // exclude paths (Connection Lines) and include rect (Linking Phrases)
});

$groups.each(function () {
  // returns only tspans !='t' --> cmap-svg contains 't'-tspans instead of blanks
  var $tspan = $(this) // https://stackoverflow.com/questions/18317082/filter-elements-out-of-a-jquery-object-based-on-text-content
    .find("tspan")
    .filter(function () {
      return this.innerHTML != "t";
    });
  var conceptText = "";
  for (let i = 0; i < $tspan.length; i++) {
    conceptText += $tspan[i].textContent;
  }

  $(this)[0].setAttribute("class", "concept"); //set class to rect elements for scroll highlight
  $(this)[0].classList.add(conceptText); //set class to rect elements for scroll highlight
  $(this).find("rect")[0].setAttribute("class", conceptText); // set class "conceptText" to rect elements for scroll highlight
});

$(".concept").on("mouseup", function () {
  var searchText = this.className.baseVal.replace("concept ", "");
  var conceptText = "." + searchText;

  //reset highlighted concepts in cmap
  $(".match").removeClass("highlight"); // remove class "highlight" from all elements with class "match"
  $(".match").removeClass("match"); // remove class match from all elements with class "match"

  var $section = $(".inhalt " + conceptText); // jQuery for elements with classes inhalt & conceptText and define as $section
  $section.addClass("match"); // add class match to $section

  $("#previous").show();
  $("#next").show();

  if ($section[0] != undefined) {
    // if $section is defined scroll to element
    scrollToSection(0, $section);
    $("#elementCount").text("1 of " + $(".match").length + " matches");
  } else {
    // if $section is not defined highlight rect/text
    $(this).find("rect:last").addClass("blink"); //option for blinking rectangle instead of text
    //$(this).find("text").addClass("blink"); //option for blinking text instead of rectangle

    setTimeout(function () {
      $(".blink").removeClass("blink");
    }, 2000);
    $("#elementCount").text("");
    //$("#elementCount").hide(); // hide text element count if no match found
    $("#previous").hide(); // hide buttons if no match found
    $("#next").hide(); // hide buttons if no match found
  }
  // adds class highlight to matching class
  $section.eq(0).addClass("highlight"); // add class "highlight" to highlight first element of $section

  //reset jQuery event of buttons
  $("#next").off(); //remove jQuery event function of "next" button
  $("#previous").off(); //remove jQuery event function of "previous" button

  var i = 0; // counter variable

  $("#next").on("mouseup", function () {
    // add jQuery event function to "next" button
    if (i >= $(".match").length - 1) {
      // if the current i is greater or equal then the length of class "match" go to the first result of the class "match" (reset i to 0)
      i = 0;
    } else {
      //else i +1
      i++;
    }
    $("#elementCount").text(i + 1 + " of " + $(".match").length + " matches");
    $(".match").removeClass("highlight"); //removes class "highlight" from "match" class elements
    $(".match").eq(i).addClass("highlight"); // add class "highlight" to the current result
    scrollToSection(i, $(".match")); // scroll to the i-th element of "match" class elements
    //$('#next').data('i', i); // set data attribute i to "next" button
  });

  $("#previous").on("mouseup", function () {
    // add jQuery event function to "next" button
    if (i === 0) {
      // if the current i is equal to zero go to the last result of the class "match" (set i to last result)
      i = $(".match").length - 1;
    } else {
      //else i +1
      i--;
    }
    $("#elementCount").text(i + 1 + " of " + $(".match").length + " matches");
    $(".match").removeClass("highlight"); //removes class "highlight" from "match" class elements
    $(".match").eq(i).addClass("highlight"); // add class "highlight" to the current result
    scrollToSection(i, $(".match")); // scroll to the i-th element of "match" class elements
  });
});

function scrollToSection(i, selector) {
  // scroll function
  var windowHeight = window.innerHeight; // define inner window height
  var sectionTop = selector[i].offsetTop; // define top of matched element
  var scrollPosition = sectionTop - windowHeight / 2 + 1; // define middle line of matched element as scroll postion
  window.scrollTo({
    top: scrollPosition, // scroll to scroll position
    behavior: "smooth", // scroll smooth
  });
}
