# Concept-based-Navigation-in-e-Portfolio
### Überblick
1. [Projektgruppe und Projekt](#projektgruppe-und-projekt)
2. [Rahmenbedingungen](#rahmenbedingungen)
3. [Technologien](#technologien)
4. [Technische Umsetzung](#technische-umsetzung)
5. [Technische Voraussetzungen](#technische-voraussetzungen)
6. [Vorgehen](#vorgehen)
7. [Kollaboratives Arbeiten](#kollaboratives-arbeiten)
8. [Schwierigkeiten und Lösungen](#schwierigkeiten-und-lösungen)
9. [Weitere Forschungsideen](#weitere-forschungsideen)

### Projektgruppe und Projekt!
***
**Vorstellung**

Hi 👋, <br>
wir sind Theresa Günther, Katharina Jahn, Michelle Payer und Sandra Schneider und bearbeiten gemeinsam die Aufgabe "Concept-based navigation in e-portfolios".
<br>
<br>
<img width="350" alt="Screenshot 2022 11 23 um 12 12 27" src="https://user-images.githubusercontent.com/98404883/204843446-936e30bf-8d1c-4841-bc34-454afd458de4.png">
<br>
<br>
🔭 Unser Arbeitsauftrag und die Ziele lauten folgendermaßen:

<b>Aufgabenstellung</b>:<br>
Entwicklung und Umsetzung eines Navigationskonzepts zur einfachen Ansteuerung und zum Durchlaufen von webbasierten e-Portfolios. Anhand einer existierenden Concept-Map* kann interaktiv zwischen den darin enthaltenen Konzepten und passenden Passagen im e-Portfolio navigiert werden.

*Concept-Map wird synoym auch als CMap oder C-Map bezeichnet, ist aber identisch

<b>Ziel</b>:<br>
Unterstützung einer Concept-Map-basierten Navigation durch e-Portfolios.   
Beispiel: Alle Stellen, an denen das Konzept "Robotik" angesprochen wird, werden visuell hervorgehoben. Werden mehrere Stellen gefunden kann von Stelle zu Stelle gesprungen werden. 
Eine Navigation soll in beide Richtungen funktionieren: 
- von der Concept-Map zum Text in den e-Portfolios
- vom e-Portfolio zurück zur Concept-Map
<br>

**Endprodukt:** <br>
Highlighten (visuelle Hervorhebung) von bestimmten Begriffen/Konzepten in e-Portfolios. Werden mehrere Stellen gefunden, kann durch einen Weiter-Button von Stelle zu Stelle gesprungen werden. Nicht vorhandene Konzepte werden mit einem optischen Feedback versehen.<br>
Das Portfolio, welches wir zum Testen verwendet haben, ist in diesem Repository auffindbar. Außerdem kann ein Blick auf die Testwebseite über diesen Link geworfen werden: https://sage-valkyrie-96c805.netlify.app/
  
<br>

**Gesamtüberblick unserer Aufgaben und Ziele als Miroboard:** <br>
 https://miro.com/app/board/uXjVOc8n0i4=/


### Rahmenbedingungen
***
**Definitorische Abgrenzungen** 

<b>Concept-Map</b>:  
Unter Concept-Maps versteht man zweidimensionale Strukturdarstellungen von Wissen oder Informationen in Form eines Netzwerkes. Das Netzwerk besteht aus Konzepten und Relationen (Gilarski, Müller & Nissen, 2020).
<br><br>
<img width="257" alt="Bildschirmfoto 2022 11 19 um 12 12 27" src="https://user-images.githubusercontent.com/98404883/202715865-d113ffc1-6730-4304-b99b-f8d60667dc9d.png"> <img width="257" alt="Bildschirmfoto 2022 11 18 um 12 12 27" src="https://user-images.githubusercontent.com/98404883/202715873-93f398fe-afd4-4309-a9bb-fbfc26a068f9.png">
<br>

<b>e-Portfolio</b>: <br>
 Ein e-Portfolio ist eine Software mit verschiedenen Dokumenten und Datenarten (Texte, Grafiken, Töne, Videos). 

"e-Portfolios stellen eine digitale Form eines Portfolios dar, bei der das Repertoire der computergestützten und web-basierten Medien zur Umsetzung genutzt werden kann. Dazu können unter anderem Medienformen wie Blogs und Profilseiten verwendet werden. Zusätzlich lassen sich auch mit Hilfe von Hyperlinks Verknüpfungen zu anderen Materialien oder Web-Seiten, z.B. zu Seiten mit Referenzen oder auch zu Seiten anderer Lernenden, einfügen. Darüber hinaus können auch Bilder, Filme und andere Medien direkt in das Portfolio eingebunden werden. Dennoch unterscheiden sich die e-Portfolios mehr durch ihre Form von klassischen Portfolios als durch die Inhalte." (Pädagogische Hochschule Weingarten, 2020).

Beispielhafte e-Portfolios, deren Navigation und Struktur wir als Orientierung und Grundlage für unser Projekt genutzt haben: 
- https://serdareviceportfolioit3.wordpress.com/
- https://alexandraseportfolioit3.wordpress.com/ 
- https://spournaraseportfolioit3.wordpress.com
<br>

**Weitere Projekte unserer Kohorte, welche für unser Projekt relevant waren bzw. welche als Voraussetzung für unser Projekt dienen:**  
 
 **Gruppe 5:** <br>
• D3-based visualisation of concept-maps with import from concept 
map file format (CMapTools)<br>
• A headless command-line tool (NodeJS, Python, ...)<br>
• Use of automatic layouters<br>
<br>
**Gruppe 8:**<br>
• Zuordnen der Wörter aus dem Portfolio zu Kategorien <br>
• Die Wörter werden dann je nach Kategorie farblich hinterlegt <br>
• Beispiel: Wort "Location" und dann wird der Ort blau hinterlegt

### Technologien
***
Genutzte Technologien für unser Projekt (allgemein) in chronologischer Reihenfolge:
* [Zoom](https://zoom.us) 
* [GoogleDocs](https://www.google.de/intl/de/docs/about/)
* [Netlify](https://www.netlify.com)
* [CMapTools](https://cmap.ihmc.us/)
* [inkspace](https://inkscape.org/de/)  
* [Brackets](https://brackets.io/)
* [Atom](https://atom.io)
* [HTML](https://www.w3.org/html/)
* [CSS](https://www.w3schools.com/css/)
* [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
* [Github](https://github.com/)
* [GitLab](https://gitlab.com/aisop/concept-based-navigation-in-e-portfolio)

### Technische Umsetzung
   ***
Unser Programm sollte zwei Hauptfunktionen erfüllen können. Zum einen sollte beim Durchscrollen des Portfolios, die dazu passende Cmap an der entsprechenden Stelle bzw. dem passenden Konzept markiert werden. Zum Anderen sollte die Cmap klickbar werden und bei einem Klick auf ein ausgesuchtes Konzept dieses im Portfolio markieren und dort hinspringen.

**1. Scrollen - Highlighten in Concept-Map: (scrollHighlight.js)**<br>
Scrollen ist ein Lehnwort aus dem Englischen. Im Original beschreibt Scrollen das vertikale und horizontale Bewegen von Texten, Bildern und Bewegtbildern innerhalb eines Monitors bei stationären oder Displays bei mobilen Endgeräten. (Quelle: https://de.ryte.com/wiki/Scrollen)<br>

Für diesen Zweck wurde von uns eine Position festgelegt (Linie auf der Mitte des Bildschirms), bei der, wenn eine Section diese überscrollt, die Klasse active an das Konzept innerhalb der Cmap ergänzt wird. Dadurch wird dieses innerhalb der Cmap farblich markiert. Natürlich wird diese Klasse wieder entfernt werden, sobald das Scrollen weitergeführt wird und zu einer anderen Sektion kommt.<br><br>

**2. Sprungeffekt - Highlighten in Konzepten: (navigation.js)**<br>
Diese Funktion ist aus mehreren Aspekten zusammengesetzt. Beim Klick auf ein Konzept innerhalb der Cmap werden verschiedene Aktionen ausgelöst.<br><br>

**Sprung zur Textstelle**<br>
Durch Klick innerhalb der Cmap auf ein Konzept springt das Portfolio an die gewünschte Stelle. Damit wichtige Punkte aus dem Portfolio direkt aufgezeigt werden, wird nach dem Klick die Klasse „.highlight“ hinzugefügt und zwar zum ersten Element dieser Auswahl.<br><br>

**Konzept nicht im Portfolio vorhanden**<br>
Sollte ein Konzept nicht innerhalb des Portfolios verknüpft sein, wurde ein optisches Feedback implementiert. Dieses färbt das Rechteck des entsprechenden Konzepts, blinkend ein. Dies geschieht eine Sekunde lang. Das kann bis auf zwei Sekunden in der css-Datei erhöht werden unter ".blink". Während dieser Zeit ist es allerdings trotzdem möglich, andere Konzepte anzuklicken.<br><br>

**Suchergebnisse auslesen und hoch-/runterzählen**<br>
Wird auf ein Konzept geklickt, springt das Portfolio nicht nur dahin, es können alle relevanten Suchergebnisse ausgelesen, gemerkt und durchgezählt werden. Wie bereits erwähnt, wird das erste Element mit der Klasse „.highlight“ versehen. Da es allerdings sein kann, dass mehrere Abschnitte, Überschriften etc. diesen Namen tragen, ist es möglich durch diese mit dem erscheinenden Navigationsbutton zu navigieren.<br><br>

**Einblenden Weiter- und Zurückbutton**<br>
Die Buttons sind also sehr wichtig. Zu Beginn der Ausführung des Programms sind die Buttons zur Navigation standardmäßig ausgeblendet. Sie werden auch ausgeblendet falls das Konzept nicht gefunden werden kann (ein Konzept also nicht im Portfolio verknüpt worden ist), obwohl sie bereits erschienen sind.<br>
Mit einem Klick auf ein vorhandenes Konzept in der Cmap, werden die Buttons für die Suchergebnisse eingeblendet.

### Technische Voraussetzungen
***
Was sind die Voraussetzungen dafür, dass unser Programm überhaupt läuft?
Im Folgenden zeigen wir die technischen Voraussetzungen auf, welche benötigt werden, damit unser Projekt erfolgreich verwendet werden kann. 
<br>

**1. Cmap**
<br>

Zunächst muss die Svg-Datei für das Programm vorbereitet werden mit Inkscape (Link: https://inkscape.org/de/):
<br>

**Schritt 1:** Datei öffnen

<img width="257" alt="Bildschirmfoto 2022-08-25 um 12 12 27" src="https://user-images.githubusercontent.com/101112426/186638408-db52b353-c3b5-4b2b-8fa8-5e241f55625d.png">
<img width="348" alt="Bildschirmfoto 2022-08-25 um 12 12 49" src="https://user-images.githubusercontent.com/101112426/186638491-c59ce2a4-f4a7-498e-8f2a-c84b5d5f51d7.png">
<br>

**Schritt 2:** Auswahl Dokumenteinstellungen

<img width="219" alt="Bildschirmfoto 2022-08-25 um 12 13 35" src="https://user-images.githubusercontent.com/101112426/186638659-8f826adf-7ab7-43f4-a147-a49d54f5aa86.png">
<br>

**Schritt 3:** Zuschneiden der Cmap auf Relevantes

<img width="614" alt="Bildschirmfoto 2022-08-25 um 13 42 18" src="https://user-images.githubusercontent.com/101112426/186655349-6a072d42-6c49-494e-a11d-f406e1302e56.png">
<br>

**Schritt 4:** Speichern unter

<img width="258" alt="Bildschirmfoto 2022-08-25 um 12 14 38" src="https://user-images.githubusercontent.com/101112426/186638904-80193d57-3b2d-43dd-b857-4d7dcd1875c5.png">
<br>


**Schritt 5:** Dateityp: optimiertes svg

<img width="405" alt="Bildschirmfoto 2022-08-25 um 12 15 05" src="https://user-images.githubusercontent.com/101112426/186638993-23143364-b1e8-4f06-8779-aab515cd0a6c.png">
<br>

**Schritt 6:** Auswahl der Optimierungen

<img width="322" alt="Bildschirmfoto 2022-08-25 um 12 15 36" src="https://user-images.githubusercontent.com/101112426/186639092-14738431-582f-4e4e-86f0-94e4caa8da80.png">
<img width="323" alt="Bildschirmfoto 2022-08-25 um 12 15 51" src="https://user-images.githubusercontent.com/101112426/186639144-d09dc354-d311-4a43-bd08-eadfb043953c.png">
<br>


**2. Anpassungen bei der Umsetzung des Portfolios**
<br>
Damit unser Programm läuft, müssen folgende Anpassungen vorgenommen werden:

Allgemeine Anpassungen im HTML-Code: 
<br>
- Innerhalb von ```<body>``` zu Beginn des textlichen Inhalts muss dieser mit dem Vermerk 
```<div id="inhalt" class="inhalt">``` gekennzeichnet werden.
- Am Ende dieses Inhalts muss dieses div noch geschlossen werden (```</div>```). Nach diesem Teil wird die Cmap, also der Beginn des svg-Inhalts, mit: 
 ```<div id="cmap" class="cmap">``` gekennzeichnet. Der HTML-Code der Cmap kann einfach reinkopiert werden. Nach dem Ende der eingefügten SVG ist diese mit ```</svg>``` geschlossen worden. 
 Nun müssen noch die Buttons wie folgt eingefügt werden:   
            ```<div id="searchNavigation">
            <button id="previous">Previous</button>
            <button id="next">Next</button>
            <p id="elementCount"></p>``` 
  Erst dann wird dieses div mit ```</div>``` geschlossen. 
- Folgende Skripte müssen am Ende der HTML, bevor sie mit ```</body>``` geschlossen wird, eingefügt werden:
  - Für die verwendete jQuery Version:
```<script src="https://code.jquery.com/jquery-3.5.0.js"></script>```  
  - Außerdem müssen unsere Javascript Dateien verknüpft werden:
  ```<script type="text/javascript" src="navigation.js"></script>```
  ```<script type="text/javascript" src="scrollHighlight.js"></script>```


Für die Funktion <b>Highlighten wichtiger Konzepte</b>:

HTML:  
Klassen mit dem Namen des Konzepts an Stellen setzen, die relevant dafür sind. Zu beachten ist, dass der Klassenname (sollten in der Cmap mehr als ein Wort stehen) zusammengeschrieben wird. Beispiel: Bezeichnung eines Konzepts in der Cmap lautet “Unterthema 2”, für die Bezeichnung im Text muss “Unterthema2” geschrieben werden.

- Überschriften h1/h2: 
```<h2>```
```<span class="sections Unterthema2">```Unterthema 2```</span>```
```</h2>```


- Gesamter Abschnitt: 
  ```<p>``` 
```<span class="sections Unterthema2">```Lorem ipsum dolor sit amet, consectetur adipiscing elit, Unterthema 2 sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.```</span>``` 
```</p>```


- Innerhalb eines Abschnitts:
Der Befehl steht innerhalb eines Textes und markiert die wichtige Information inmitten eines Satzes bzw. Abschnittes.
```<p>``` 
… Excepteur sint occaecat cupidatat ```<span class="sections Oberthema2">```non proident, Oberthema 2```</span>``` sint occaecat cupidatat …… 
```</p>```


Scrollen verbunden mit Markieren der Cmap:
HTML: Alle Stellen und Überschriften, die einem Konzept der Cmap zugehörig sind, sind mit der Klasse sections versehen (s.o. beispielsweise unter Punkt: innerhalb eines Abschnitts)
<br> 
<br>
**3. style.css Datei**
<br>
Für die Darstellung unseres Programms benötigt man eine style.css-Datei, hierfür haben wir die "style.css" erstellt. 
Dieses Dateiformat speichert Styling-Informationen wie Symbole, Kartenelemente, Hintergründe, Rahmen, Farbschema usw. 
Sie wird benötigt um den Inhalt einer Webseite zu formatieren. Es enthält angepasste, globale Eigenschaften für die Anzeige von HTML-Elementen. CSS-Dateien können Größe, Farbe, Schriftart, Zeilenabstand, Einzug, Ränder und Position von HTML-Elementen definieren. 
In dieser kann in unserem Fall auch die Farbe der Markierungen bestimmt werden:
- unter .active wird die Farbe bestimmt, die innerhalb der Cmap verwendet wird, um die Position des Konzepts anzuzeigen.
- unter .concept:hover rect wird die Farbe bestimmt, die beim Drüberhovern innerhalb der Cmap der äußere Rahmen des Rechtecks bekommt.
- unter .highlight wird die Farbe bestimmt, mit der das in der Cmap angeklickte Konzept innerhalb des Portfolios gehighlighted wird. 
Außerdem kann die Position der Buttons bestimmt werden:
- unter .cmap button wird die Position in Relation zur Cmap gesetzt. Soll sie unter der Cmap liegen, muss eine Position im negativen px-Bereich gewählt werden.
Der Blinkeffekt, bei einem nicht vergebenen Konzept wird unter .blink angepasst.

### Vorgehen
***
Im Folgenden zeigen wir unser zusammengefasstes Vorgehen der Gruppenarbeit und des Projekts.<br>  
1. Auseinandersetzung mit dem Arbeitsauftrag
   - Klären der Ziele und Anforderungen<br><br>  
2. Einarbeitung in die Grundlagen<br> 
   - Cmaps<br>
   - e-Portfolios<br>
   - HTML & Java<br><br>
3. Sammeln erster Ideen und Austausch über Vorschläge des Projekts <br><br>
4. Regelmäßige Gruppentreffen mit Austausch über:
   - Erstellen eines e-Portfolios (Webseite) in HTML <br>
   - Erstellen einer universellen Cmap (in Cmap Tools) mit hierarchischer Struktur <br>
   - Erstellung von Verknüpfung der Buttons, d.h klickbar machen und highlighten<br><br>
 
Im Folgenden zeigen wir unser detailliertes Vorgehen des Projekts. 

<b>Stand 08.12.2021</b>:
- Portfolios von Kommilitonen besorgen und vergleichen. 
  Schwerpunkt: Gibt es eine schnelle Möglichkeit zwischen den Portfolios zu wechseln und eine einheitliche Navigation? ✅
- Bewertungskriterien (Handreichung für Portfolios) anschauen ✅
- Cmap Tool downloaden und einarbeiten ✅
- Wenn bestimmte Entscheidungen getroffen werden (Technologie) sollten diese dokumentiert werden und nachvollziehbar sein ✅

<b>Stand 28.01.2022</b>: 

Brainstorming Aufgaben: 
1. Framing Cmaps Export
2. HTML Single Page Darstellung 
3. Brücke darstellen → highlighten, wie kann ich wo klicken und wo anders navigieren

Daraus enstandene Aufgaben:
- in HTML Seite bestimmte Wörter markieren (statisch → Button einbauen → Button durch Interaktion ersetzen) (durch Buttonklicks unterschiedliche Wörter selektieren/   deselektieren) → unabhängig von Cmap ✅
- Portfolio als URL an Zwischenort speichern ✅
- Speicherung der Modelle bzw. Cmap als SVG Datei exportieren, selbst ne Website schreiben wo die drin ist und anklickbar machen 
- Verbindung miteinander (Buttons durch Cmap ersetzen) ✅

<b>Stand 18.02.2022</b>:
- einfache Cmap gestalten (in Cmap Tools) (universell: Oberthema 1, Oberthema 2 etc.) ✅
- Code von Cmap verstehen → welche Stelle ist wo, wo kann was eingefügt werden? → Id´s identifizieren → CXL Datei anschauen ✅
- Buttons erstellen ✅
- Springen innerhalb einer Seite durch Buttondruck mit Befehl "href id" einfügen ✅
- Buttons Highlighten ✅
- Bei Druck auf anderen Button highlight etc. von vorherigem Button entfernen und neu highlighten ✅

Darüberhinaus folgende Aufgaben:
→ beide Aufgabenbereiche zusammenfügen, kombinieren und testen ✅
→ kleines universelles Java Programm schreiben ✅

<b>Stand 16.03.2022</b>:
- Buttons klickbar ✅
- Suchwörter farblich unterlegt / highlighten ✅
- ReadMe-Datei anlegen ✅

<b>Stand 18.03.2022</b>:
- größere Cmap erstellen (in Cmap Tools) (Scrollen) ✅
- ReadMe-Datei ausführlicher schreiben ✅

<b>Stand 31.03.2022</b>:
- Paragraph highlighten?  ✅
→ Ganzer Paragraph highlighten (In Portfolios sind die Paragraphen oft kürzer, oft 
     nur 2 Zeilen) 
→ Aus kompletten Paragraph können auch nur 2-3 Sätze gehighlightet werden 
    (wenn dies wichtige Sätze sind die zum Thema passen → logische Absätze) 
- Wichtig: Marker setzen aus dem die ID erkennbar ist → Welches Konzept?  ✅


<b>Stand 04.-08.2022</b>:
- Usertest für unsere 2 Navigationsmöglichkeiten (Auflistung oder Weiter-Button) erstellen  ✅
-  Github-Datei fertigstellen ✅
-  Farbkombination des User-Tests umsetzen ✅
-  Highlighten von Schlagwörtern und zugehörigem Inhalt (geht noch bis Zeilenende) ✅

<b>Stand 08.-10.2022</b>:
-  zwei Möglichkeiten der Cmap erstellen (eine längliche und eine breite) ✅
-  Befehl für Weiter-/Zurück-Button, dass dieser erst beim Klicken angezeigt wird, raussuchen ✅
-  Horizontale Anordnung der C-Map ✅
-  Suchergebnis-Feld beim Weiter-/Zurück-Button einfügen ✅
-  Highlighten soll nach Wort enden, nicht bis Ende der Zeile ✅
-  Webseite für Präsentation fertigstellen ✅

  
<b>Stand 11.-12.2022</b>:
-  Cmap ergänzen und verbessern (an Webseite anpassen) ✅
-  Respository ergänzen, Korrektur lesen und ausbessern ✅
-  Präsentation üben ✅
-  Letzte Bugs gefixed und Blinken implementiert ✅
-  Bei GitLab anmelden und gesamtes Material zur Verfügung stellen ✅
  
### Kollaboratives Arbeiten
***
Im Folgenden nennen wir kurz unsere Tools zum kollaborativen Arbeiten.
1. **Zoom:**
Regelmäßige Gruppenarbeiten und Austausch über den aktuellen Stand bzw. Fortschritte.  
2. **Google Docs:**
Sammlung unserer Grundlagen, Ideen, Aufgabenverteilung und Mitschriften der Sprechstunden.  
2. **Slack:**
Kommunikation mit den Dozenten. 
3. **Github:**
Dokumentation des Projekts und gemeinsames sammeln der Ergebnisse.
4. **CMapTools Cloud:**
Online Austausch und Erstellung einer gemeinsamen Cmap.

### Schwierigkeiten und Lösungen
***
Da wir alle nicht sonderlich viel Erfahrung in dem Gebiet haben, stellte uns dieses Projekt vor einige Schwierigkeiten. Im Folgenden zählen wir einige auf:
<br>
<br>
  <b>Cmap</b>:

- Eigene Cmap von Cmap Tools speichern:  
Cmap in Cmap Tools erstellen → Auf Datei mit Rechtsklick klicken → Speichern → unter Meine Cmaps gespeichert und kann bei Bedarf weiterbearbeitet werden.
Um Cmap-Datei in einem neuen Format speichern zu wollen auf Datei klicken → Exportieren als und das gewünschte Dateiformat auswählen (z.B. PDF, JPEG, Webseite, SVG etc.)

- Cmap für weitere Personen sichtbar machen / teilen bzw. zusammen an einer Cmap arbeiten: 
 über Cmap Cloud möglich (https://cmapcloud.ihmc.us/login.html). Hierfür muss eigene Cmap im gewünschten Format gespeichert werden und dann in Cmap Cloud bei geteiltem Projekt hochgeladen werden. 
<br>

<b>Technischer Aspekt</b>:

Zu Beginn war die Frage, wie die Cmap und unser Programm überhaupt in Verbindung gebracht werden können. Der Aufbau der Cmap, nachdem die Map eingefügt wurde, war sehr schwierig. Es gibt viele g-Tags also groups. Allerdings gibt es immer ein bestimmtes Muster, dem die Anordnung folgt: s. Navigation.js (Zeile 6-17).

Aus diesem Grund konnten wir ausmachen, dass wir groups mit einem rect element suchen müssen, aus dem unser Programm die Bezeichnung des Konzepts herauslesen kann.
 
- Scrollen: 
  - Was soll bei der Cmap beim Scrollen markiert werden? Markieren innerhalb der Cmaps ist abhängig von dem Design, welches die Cmap in Cmap Tools erhält. Da wir uns       die Rechtecke (rectangles) aus der Cmap raussuchen, die gehighlightet werden sollen, darf das Design nicht von dem beschriebenen Design der Cmap abweichen (s. Schriftgröße, Größe und Form des Rechtecks, Dicke des Rechteckrandes, Schattierung des Rechtecks): <img width="808" alt="Bildschirmfoto 2022-12-06 um 13 45 12" src="https://user-images.githubusercontent.com/101112426/205916246-33ee2ea4-6504-42ed-993c-9a97730fd640.png">
 
  - Ab wann soll ein Konzept markiert werden und als "sichtbar" gelten? Beim Scrollen durch das Portfolio wird irgendwann ein Konzept als das definiert, welches gerade     vermutlich gelesen wird. Dafür haben wir uns entschieden, die Hälfte des Bildschrims zu verwenden, welche abhängig vom verwendeten Bildschirm des Users ist.

- Highlighten im Text:
  - Funktionierende Vor- und Zurückbutton um bei Auswahl eines interessanten Konzepts, durch die Suchergebnisse zu navigieren: Die Liste der Suchergebnisse muss sich       das Programm merken und das Highlighten eines alten Suchbegriffs "löschen". Das hat einige Zeit gedauert, wir konnten das aber folgendermaßen lösen: s. Navigation.js (Zeile 68-83).
  

### Weitere Forschungsideen
***
Im Folgenden nennen wir unsere persönlichen Überlegungen für die Weiterarbeit mit unserem Projekt.
Hinweis: Diese Ideen sollen lediglich als Anregung dienen und stellen kein Muss dar!

- <b>Weiterarbeit mit dem Portfolio an sich</b>:
   - Klicken im Portfolio an bestimmte Stelle und anschließendes Highlighten in der Cmap an entsprechender Stelle. 

- <b>Weiterarbeit für Studierende</b>:
   - Anstelle der Navigationsmöglichkeit mit dem "Zurük-/Weiter-Button" ein Programm für die Navigationsmöglichkeit "Auflistung" (s. Anlage - Usertest) schreiben. 
  - weitere Usertests zu Farben & Farbkombinationen und Sinnhaftigkeit & Ausprägungen. 



